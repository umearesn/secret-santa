using SecretSanta.ConnectionFactory;

namespace SecretSanta._DI;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
    {
        services.ConfigureOptions<PgConnectionOptions>(configuration);
        services.AddSingleton<ConnectionFactory.ConnectionFactory>();

        return services;
    }

    private static void ConfigureOptions<TOptions>(this IServiceCollection services, IConfiguration configuration)
        where TOptions : class
    {
        var optionsSection = configuration.GetSection(typeof(TOptions).Name);
        services.Configure<TOptions>(optionsSection);
    }
}
