namespace SecretSanta.ConnectionFactory;

public class PgConnectionOptions
{
    public string ConnectionString { get; set; }
}
