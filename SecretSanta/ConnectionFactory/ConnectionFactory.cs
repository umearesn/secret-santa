using System.Data;
using Microsoft.Extensions.Options;
using Npgsql;

namespace SecretSanta.ConnectionFactory;

public class ConnectionFactory
{
    private readonly string _connectionString;

    public ConnectionFactory(IOptions<PgConnectionOptions> dbOptions)
    {
        _connectionString = dbOptions.Value.ConnectionString;
    }

    public IDbConnection GetDbConnection()
        => new NpgsqlConnection(_connectionString);
}
