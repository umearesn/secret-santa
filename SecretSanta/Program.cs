using Microsoft.OpenApi.Models;
using SecretSanta._DI;
using SecretSanta.DAL;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;
var configuration = builder.Configuration;

services.AddControllers();
services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "SecretSanta", Version = "v1"}); });
services.AddEndpointsApiExplorer();

services.ConfigureDatabase(configuration);
services.AddScoped<GroupRepository>();

var app = builder.Build();
app.UseDeveloperExceptionPage();
app.UseSwagger();
app.UseSwaggerUI();


app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "FeedReader v1 PROD");
});


app.UseRouting();
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

app.Run();


