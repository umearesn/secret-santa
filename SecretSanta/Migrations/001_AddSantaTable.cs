using SimpleMigrations;

namespace RealEstateParser.Migrations.Migrations;

[Migration(001, "Schema for santa storing")]
public class _001_AddSantaSchema : Migration {

    protected override void Up()
    {
        const string query = @"
            CREATE SCHEMA IF NOT EXISTS santa;

            CREATE TABLE IF NOT EXISTS santa.""group""
            (
                id          SERIAL PRIMARY KEY,
                name        TEXT NOT NULL,
                description TEXT
            );

            CREATE TABLE IF NOT EXISTS santa.participant
            (
                id           SERIAL PRIMARY KEY,
                name         TEXT    NOT NULL,
                wish         TEXT,
                recipient_id INTEGER NOT NULL REFERENCES santa.participant (id)
            );


            CREATE TABLE IF NOT EXISTS santa.group_participant
            (
                group_id       INTEGER REFERENCES santa.""group"" (id),
                participant_id INTEGER REFERENCES santa.participant (id)
            );";

        Execute(query);
    }

    protected override void Down()
    {
        Execute(@"drop schema santa cascade;");
    }
}
