using Dapper;
using SecretSanta.DAL.Models;
using SecretSanta.DAL.Models.Group;

namespace SecretSanta.DAL;

public sealed class GroupRepository
{
    private readonly ConnectionFactory.ConnectionFactory _connectionProvider;

    public GroupRepository(
        ConnectionFactory.ConnectionFactory connectionProvider)
    {
        _connectionProvider = connectionProvider;
    }

    public async Task<CreateGroup.Response> CreateGroup(
        CreateGroup.Request req,
        CancellationToken ct)
    {
        var (name, description) = req;

        const string query = @"
            INSERT INTO santa.""group""(name, description)
            VALUES (:name, :description)
            RETURNING id;";

        using var connection = _connectionProvider.GetDbConnection();
        var id =
            (await connection.QueryAsync<long>(
                new CommandDefinition(
                    query,
                    new
                    {
                        name,
                        description
                    },
                    cancellationToken: ct)))
            .First();

        return new CreateGroup.Response(id);
    }

    public async Task<GetGroupByIds.Response> GetGroupById(
        GetGroupByIds.Request req,
        CancellationToken ct)
    {
        var ids = req.Ids.ToArray();

        const string query = @"
            SELECT id AS Id, name AS Name, description AS Description
            FROM santa.""group""
            WHERE id = ANY (:ids);";

        using var connection = _connectionProvider.GetDbConnection();
        var groups =
            (await connection.QueryAsync<GroupModel>(
                new CommandDefinition(
                    query,
                    new
                    {
                        ids
                    },
                    cancellationToken: ct)))
            .ToList();

        return new GetGroupByIds.Response(groups);
    }

    public async Task<GetGroups.Response> GetGroups(
        CancellationToken ct)
    {
        const string query = @"
            SELECT id AS Id, name AS Name, description AS Description
            FROM santa.""group"";";

        using var connection = _connectionProvider.GetDbConnection();
        var groups =
            (await connection.QueryAsync<GroupModel>(
                new CommandDefinition(
                    query,
                    cancellationToken: ct)))
            .ToList();

        return new GetGroups.Response(groups);
    }

    public async Task<UpdateGroup.Response> UpdateGroup(
        UpdateGroup.Request req,
        CancellationToken ct)
    {
        var (id, name, description) = req.GroupToUpdate;

        const string query = @"
            UPDATE santa.""group""
            SET name        = :name,
            description = :description
            WHERE id = :id
            RETURNING id;";

        using var connection = _connectionProvider.GetDbConnection();
        var groups =
            (await connection.QueryAsync<long>(
                new CommandDefinition(
                    query,
                    new
                    {
                        name,
                        description,
                        id
                    },
                    cancellationToken: ct)))
            .ToHashSet();

        return new UpdateGroup.Response(groups.Contains(id));
    }

    public async Task<DeleteGroup.Response> DeleteGroup(
        DeleteGroup.Request req,
        CancellationToken ct)
    {
        var id = req.Id;

        const string query = @"
            DELETE
            FROM santa.""group""
            WHERE id = :id
            RETURNING id;";

        using var connection = _connectionProvider.GetDbConnection();
        var groups =
            (await connection.QueryAsync<long>(
                new CommandDefinition(
                    query,
                    new
                    {
                        id
                    },
                    cancellationToken: ct)))
            .ToHashSet();

        return new DeleteGroup.Response(groups.Contains(id));
    }
}
