namespace SecretSanta.DAL.Models;

public sealed record GroupModel(
    long Id,
    string Name,
    string? Description)
{
    private GroupModel() : this(default, string.Empty, default)
    {
    }
}
