namespace SecretSanta.DAL.Models.Group;

public sealed record GetGroupByIds
{
    public sealed record Request(
        IReadOnlySet<long> Ids);

    public sealed record Response(
        IReadOnlyList<GroupModel> Groups);
}
