namespace SecretSanta.DAL.Models.Group;

public sealed record DeleteGroup
{
    public sealed record Request(
        long Id);

    public sealed record Response(
        bool IsFound);
}
