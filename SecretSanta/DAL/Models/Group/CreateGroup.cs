namespace SecretSanta.DAL.Models.Group;

public sealed record CreateGroup
{
    public sealed record Request(
        string Name,
        string? Description);

    public sealed record Response(
        long Id);
}
