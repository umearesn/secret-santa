namespace SecretSanta.DAL.Models.Group;

public sealed record UpdateGroup
{
    public sealed record Request(
        GroupModel GroupToUpdate);

    public sealed record Response(
        bool IsFound);
}
