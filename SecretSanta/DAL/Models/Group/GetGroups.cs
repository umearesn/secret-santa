namespace SecretSanta.DAL.Models.Group;

public sealed record GetGroups
{
    public sealed record Response(
        IReadOnlyList<GroupModel> Groups);
}
