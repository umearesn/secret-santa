namespace SecretSanta.DAL.Models;

public sealed record ParticipantModel(
    long Id,
    string Name,
    string? Wish,
    long RecipientId)
{
    private ParticipantModel() : this(default, string.Empty, default, default)
    {
    }
}
