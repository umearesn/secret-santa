using System.Text.Json.Serialization;

namespace SecretSanta.Controllers.Models;

public sealed class CreateGroup
{
    [JsonPropertyName("name")]
    public string? Name { init; get; }

    [JsonPropertyName("description")]
    public string? Description { init; get; }
}

public sealed class UpdateGroupRequest
{
    [JsonPropertyName("name")]
    public string? Name { init; get; }

    [JsonPropertyName("description")]
    public string? Description { init; get; }
}


public sealed class ContractGroup
{
    [JsonPropertyName("id")]
    public long Id { init; get; }

    [JsonPropertyName("name")]
    public string Name { init; get; } = null!;

    [JsonPropertyName("description")]
    public string? Description { init; get; }
}

public sealed class ContractGroupFullInfo
{
    [JsonPropertyName("id")]
    public long Id { init; get; }

    [JsonPropertyName("name")]
    public string Name { init; get; } = null!;

    [JsonPropertyName("description")]
    public string? Description { init; get; }

    [JsonPropertyName("participants")]
    public IReadOnlyList<ContractParticipant> Participants { init; get; } = Array.Empty<ContractParticipant>();
}


public sealed class ContractParticipant
{
    [JsonPropertyName("id")]
    public long Id { init; get; }

    [JsonPropertyName("name")]
    public string Name { init; get; } = null!;

    [JsonPropertyName("wish")]
    public string? Wish { init; get; } = null!;

    [JsonPropertyName("recipient")]
    public ContractParticipant? Recipient { init; get; }
}
