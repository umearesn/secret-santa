using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Mvc;
using SecretSanta.Controllers.Models;
using SecretSanta.DAL;
using SecretSanta.DAL.Models;
using SecretSanta.DAL.Models.Group;
using CreateGroup = SecretSanta.Controllers.Models.CreateGroup;

namespace SecretSanta.Controllers;

[ApiController]
[Produces("application/json")]
public class Controller : ControllerBase
{
    private readonly GroupRepository _groupRepository;

    public Controller(
        GroupRepository groupRepository)
    {
        _groupRepository = groupRepository;
    }

    [HttpPost("group")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> CreateGroup(
        [FromBody] CreateGroup req,
        CancellationToken ct)
    {
        if (string.IsNullOrEmpty(req.Name))
        {
            return BadRequest("Пустое имя группы недопустимо!");
        }

        await _groupRepository.CreateGroup(
            new DAL.Models.Group.CreateGroup.Request(
                req.Name,
                req.Description),
            ct);

        return Ok();
    }


    [HttpGet("groups")]
    [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(IReadOnlyList<ContractGroup>))]
    public async Task<ActionResult<IReadOnlyList<ContractGroup>>> GetGroups(
        CancellationToken ct)
    {
        var groups =
            await _groupRepository.GetGroups(ct);


        return groups.Groups
            .Select(x => new ContractGroup()
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToList();
    }

    [HttpGet("group/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(ContractGroupFullInfo))]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<ContractGroupFullInfo>> GetGroup(
        long id,
        CancellationToken ct)
    {
        var groups =
            await _groupRepository.GetGroupById(
                new GetGroupByIds.Request(new HashSet<long>() {id}),
                ct);

        if (groups.Groups.Any(x => x.Id == id) is false)
        {
            return NotFound($"Группа {id} не найдена!");
        }

        var group = groups.Groups.First(x => x.Id == id);

        return new ContractGroupFullInfo()
        {
            Id = group.Id,
            Name =group.Name,
            Description = group.Description,
            Participants = new List<ContractParticipant>()
        };
    }


    [HttpPut("group/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateGroup(
        long id,
        [FromBody] UpdateGroupRequest req,
        CancellationToken ct)
    {
        if (string.IsNullOrEmpty(req.Name))
        {
            return BadRequest("Обновить имя группы на пустое недопустимо!");
        }

        var isFound =
            (await _groupRepository.UpdateGroup(
                new UpdateGroup.Request(
                    new GroupModel(
                        id,
                        req.Name,
                        req.Description)),
                ct))
            .IsFound;

        return isFound ? Ok() : NotFound($"Группа {id} не найдена!");
    }

    [HttpDelete("group/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteGroup(
        long id,
        CancellationToken ct)
    {
        var isFound =
            (await _groupRepository.DeleteGroup(
                new DeleteGroup.Request(id),
                ct))
            .IsFound;

        return isFound ? Ok() : NotFound($"Группа {id} не найдена!");
    }
}
